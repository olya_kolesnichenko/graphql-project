import gql from 'graphql-tag';

export const POST_QUERY = gql`
  query postQuery($filter: String, $skip: Int, $first: Int, $orderBy: PostOrderByInput) {
    posts(filter: $filter, skip: $skip, first: $first, orderBy: $orderBy) {
      count
      postList {
        id
        text
        likeCount
        dislikeCount
        comments {
          id
          text
        }
      }
    }
  }
`;

export const POST_POST_MUTATION = gql`
  mutation PostMutation($text: String!) {
    postPost(text: $text) {
      id
      text
      likeCount
      dislikeCount
      comments {
        id
        text
      }
    }
  }
`;

export const POST_POST_MUTATION_UPDATE = gql`
  mutation PostMutationUpdate($id: ID!, $text: String, $likeCount: Int, $dislikeCount: Int) {
    updatePost(id: $id, text: $text, likeCount: $likeCount, dislikeCount: $dislikeCount) {
      id
      text
      likeCount
      dislikeCount
      comments {
        id
        text
      }
    }
  }
`;

export const POST_COMMENT_MUTATION = gql`
  mutation PostMutation($postId: ID!, $text: String!) {
    postComment(postId: $postId, text: $text) {
      id
      text
    }
  }
`;

export const NEW_POSTS_SUBSCRIPTION = gql`
  subscription {
    newPost {
      id
      text
      likeCount
      dislikeCount
      comments {
        id
        text
      }
    }
  }
`;