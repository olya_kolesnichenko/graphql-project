import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_QUERY, POST_COMMENT_MUTATION } from '../../queries';

const CommentForm = props => {
  const { postId, toggleForm } = props;
  const [text, setText] = useState('');

  const _updateStoreAfterAddingComment = (store, newComment, postId) => {
    const orderBy = 'createdAt_DESC';
    const data = store.readQuery({
      query: POST_QUERY,
      variables: {
        orderBy
      }
    });
    const commentedPost = data.posts.postList.find(
      item => item.id === postId
    );
      commentedPost.comments.push(newComment);
    store.writeQuery({ query: POST_QUERY, data });
    toggleForm(false);
  };

  return (
    <div className="form-wrapper">
      <div className="input-wrapper">
        <textarea
          onChange={e => setText(e.target.value)}
          placeholder="Comment text"
          autoFocus
          value={text}
          cols="25"
        />
      </div>
      <Mutation
        mutation={POST_COMMENT_MUTATION}
        variables={{ postId, text }}
        update={(store, { data: { postComment } }) => {
          _updateStoreAfterAddingComment(store, postComment, postId)
        }}
      >
        {postMutation =>
          <button onClick={postMutation}>Add comment</button>
        }
      </Mutation>
    </div>
  );
};

export default CommentForm;