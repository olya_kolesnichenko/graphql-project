import React from 'react';

const CommentItem = props => {
    const { text } = props;
    return (
        <p>{text}</p>
    );
};

export default CommentItem;