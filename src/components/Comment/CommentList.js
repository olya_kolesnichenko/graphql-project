import React, { useState } from 'react';
import CommentItem from './CommentItem';
import CommentForm from './CommentForm';

const CommentList = props => {
    const [showCommentForm, toggleForm] = useState(false);
    const { postId, comments } = props;
    
    return (
        <div className="comment-list">
            {comments.length > 0 && <span className="comment-list-title">Comments</span>}
            {comments.map(item => {
                return <CommentItem key={item.id} {...item} />
            })}
            <button className="comment-button" onClick={() => toggleForm(!showCommentForm)}>
                {showCommentForm ? 'Close comment' : 'Add comment'}
            </button>
            {showCommentForm && <CommentForm
                postId={postId}
                toggleForm={toggleForm}
            />}
        </div>
    );
}

export default CommentList;