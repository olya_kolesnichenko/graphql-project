import React from 'react';
import CommentList from '../Comment/CommentList';
import { Mutation } from 'react-apollo';
import { POST_POST_MUTATION_UPDATE } from '../../queries';

const PostItem = props => {
  const { id, text, likeCount, dislikeCount, comments } = props;

  return (
    <div className="post-item">
      <div className="title-wrapper">
        <h2>{"#"+id.substr(id.length-5)}</h2>
          <p>{text}</p>
          <div className="buttons">
              <span>
                  <Mutation
                      mutation={POST_POST_MUTATION_UPDATE}
                      variables={{ id, likeCount: likeCount+1 }}
                  >
                    {postMutationUpdate =>
                    <i className="icon like-icon"  onClick={postMutationUpdate}></i>
                    }
                  </Mutation>
                  {likeCount} </span>
              <span><Mutation
                  mutation={POST_POST_MUTATION_UPDATE}
                  variables={{ id, dislikeCount: dislikeCount+1 }}
              >
                    {postMutationUpdate =>
                        <i className="icon dislike-icon"  onClick={postMutationUpdate}></i>
                    }
                  </Mutation>
                  {dislikeCount} </span>
          </div>
      </div>
      <CommentList postId={id} comments={comments} />
    </div>
  );
};

export default PostItem;