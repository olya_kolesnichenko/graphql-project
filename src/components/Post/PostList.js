import React, { useState } from 'react';
import { Query } from 'react-apollo';
import PostItem from './PostItem';
import { POST_QUERY, NEW_POSTS_SUBSCRIPTION } from '../../queries';

const PostList = props => {
  const orderBy = 'createdAt_DESC';
    const POSTS_ON_PAGE = 10;
    let  pages;
    const [order, setOrder] = useState(orderBy);
    const [filterQuery, setFilter] = useState('');
    const [skip, setSkip] = useState(0);


  const _subscribeToNewPosts = subscribeToMore => {
    subscribeToMore({
      document: NEW_POSTS_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newPost } = subscriptionData.data;
        const exists = prev.posts.postList.find(({ id }) => id === newPost.id);
        if (exists) return prev;

        return {...prev, posts: {
          postList: [newPost, ...prev.posts.postList],
          count: prev.posts.postList.length + 1,
          __typename: prev.posts.__typename
        }};
      }
    });
  };
     const _handleKeyPress = function(e){
        setFilter(e.target.value);
     };

    const pagination = function (countPages) {
       let buttons = [];
        for (let i=1; i<=countPages; i++){
          const b = <button key={i} onClick={e => setSkip(i*10-10)}>{i}</button>;
            buttons.push(b)
        }
        return buttons;
    };


  return (
      <section className="post-list">
      <select defaultValue="createdAt_DESC" onChange={e => setOrder(e.target.value)}>
        <option value='createdAt_DESC'>by createdAt DESC</option>
        <option value='createdAt_ASC'>by createdAt ASC</option>
        <option value='likeCount_DESC'>by likeCount DESC</option>
        <option value='likeCount_ASC'>by likeCount ASC</option>
        <option value='dislikeCount_DESC'>by dislikeCount DESC</option>
        <option value='dislikeCount_ASC'>by dislikeCount ASC</option>
      </select>

      <input type="text" placeholder="filter" onKeyUp={e => _handleKeyPress(e)}/>

    <Query query={POST_QUERY} variables={{ filter: filterQuery, skip, first: POSTS_ON_PAGE, orderBy: order }}>
      {({ loading, error, data, subscribeToMore }) => {
        if (loading) return <div>Loading...</div>;
        if (error) return <div>Fetch error</div>;
        _subscribeToNewPosts(subscribeToMore);

        const { posts: { postList, count } } = data;
        if (count > POSTS_ON_PAGE){
          pages = (count % POSTS_ON_PAGE !== 0) ? parseInt(count / POSTS_ON_PAGE)+1 : (count / POSTS_ON_PAGE);
          let paginationButtons = pagination(pages);
        }

        return (
            <section>

          <div>
            {postList.map(item => {
              return <PostItem key={item.id} {...item} />
            })}
          </div>
                {pages && <div className="pagination">
                    {pagination(pages)}

                </div>}

            </section>
        );
      }}
    </Query>

      </section>
  );
};

export default PostList;