import React, { useState } from 'react';
import { Mutation } from 'react-apollo';
import { POST_POST_MUTATION} from '../../queries';

const PostForm = props => {
  const [text, setText] = useState('');

  const _updateStoreAfterAddingPost = (store, newPost) => {
     setText("")
  };

  return (
    <div className="form-wrapper post-form">
      <div className="input-wrapper">
        <input type="text" placeholder="Type something" value={text} onChange={e => setText(e.target.value)} />
      </div>

      <Mutation
        mutation={POST_POST_MUTATION}
        variables={{ text }}
        update={(store, { data: { postPost } }) => {
          _updateStoreAfterAddingPost(store, postPost);
        }}
      >
        {postMutation =>
          <button className="post-button" onClick={postMutation}>Add post</button>
        }
      </Mutation>
    </div>
  );
};

export default PostForm;