import React from 'react';
import { Link, Route, Switch } from 'react-router-dom';
import PostList from './Post/PostList';
import PostForm from './Post/PostForm';
import './App.scss';

function App() {
  return (
    <div className="App">
      <div className="nav-panel">
        <Link to="/">Anonymous chat</Link>
          {/*<Link to="/add-product">Add product</Link>*/}
      </div>
        <PostForm/>
        <PostList/>

        {/*<Switch>
            <Route exact path="/" component={PostList}/>
            { <Route exact path="/add-product" component={PostForm} /> }
        </Switch>*/}
    </div>
  );
}

export default App;
