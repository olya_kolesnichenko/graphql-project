function postPost(parent, args, context, info) {
  return context.prisma.createPost({
    text: args.text,
    likeCount: 0,
    dislikeCount: 0
  });
}

async function updatePost(parent, args, context, info) {
    const postExists = await context.prisma.$exists.post({
        id: args.id
    });

    if (!postExists) {
        throw new Error(`Post with ID ${args.id} does not exist`);
    }

    const likeCount = args.likeCount ? args.likeCount : postExists.likeCount ;
    const dislikeCount = args.dislikeCount ? args.dislikeCount :postExists.dislikeCount;

    return context.prisma.updatePost({
        data: {likeCount: likeCount, dislikeCount: dislikeCount} ,
        where: {id :args.id}
    });
}

async function postComment(parent, args, context, info) {
  const postExists = await context.prisma.$exists.post({
    id: args.postId
  });

  if (!postExists) {
    throw new Error(`Post with ID ${args.postId} does not exist`);
  }

  return context.prisma.createComment({
    text: args.text,
    post: { connect: { id: args.postId } }
  });
}

module.exports = {
  postPost,
  updatePost,
  postComment
};