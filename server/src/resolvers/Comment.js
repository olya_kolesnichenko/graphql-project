function post(parent, args, context) {
    return context.prisma.comment({
        id: parent.id
    }).post();
}

module.exports = {
    post
}