
### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm start-server`

Runs the GraphQL server.<br>
Open [http://localhost:4000](http://localhost:4000) to test API in GraphQL Playground.